Project contains convert amount with currency and exchange rates
There's two link in the navbar. First is convert page. Second is table with exchange rates
And when you open page first time you can select currency for exchange rate from appearing modal.
You can change it at second page

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

