import request from "./index";

export const CurrencyService = {
    getCurrencies: async ()  => {
      const { symbols } = await request({
        url: '/symbols',
        method: "GET"
      });
      
      return symbols;
    },
    convertAmount: async (to, from, amount)  => {
      const { result } = await request({
        url: `/convert?to=${to}&from=${from}&amount=${amount}`,
        method: "GET"
      });
      return result;
    },
  };