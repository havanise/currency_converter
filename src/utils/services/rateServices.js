import request from "./index";

export const RateService = {
    getRates: async (base)  => {
        const { rates } = await request({
          url: `/latest?base=${base}`,
          method: "GET"
        });
        
        return rates;
      },
  };