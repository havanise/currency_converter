import axios from "axios";

const client = (() => {
  return axios.create({
    baseURL: 'https://api.apilayer.com/exchangerates_data',
    headers: {
        "apikey": "yfwn4SbcFEj3jQTglIaEoG2rQ5fAX9Hj"
      }
  });
})();


const request = async function (options, store) {
  // success handler
  const onSuccess = function (response) {
    const { data } = response;
    return data;
  };

  // error handler
  const onError = function (error) {
    return Promise.reject(error.response);
  };

  // adding success and error handlers to client
  return client(options).then(onSuccess).catch(onError);
};

export default request;
