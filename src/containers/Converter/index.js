import React, {useState} from 'react';
import { connect } from "react-redux";
import { useQuery } from "react-query";
import {CurrencyService} from '../../utils/services/currencyService';
import CurrencySelect from '../../components/CurrencySelect';
import './index.css';

const customStyle = { top: 90 };

function Converter({ isCurrencyLoading, currencies }) {
    const [state, setState] = useState({
        From: undefined,
        To: undefined,
        amount: 0,
        result: 0,
      });

    const { isLoading: isConvertLoading, data: convertAmount, refetch: convertAmountFunction } = useQuery(['convert'], () => CurrencyService.convertAmount(state.To || Object.keys(currencies)[0], state.From || Object.keys(currencies)[0], state.amount),  { enabled: false, refetchOnWindowFocus: false });

    const handleInput = (event) => {
        setState({
            ...state,
            amount: event.target.value || 0,
        })
    };

    const handleCurrencyChange = (event, label)=> {
        setState({
            ...state,
            [label]: event?.currentTarget?.value || undefined,
        })
    };

    const convertCurrency = () => {
        convertAmountFunction(['convert',{from: state.From}])
    };

	return (
        <section className="container flexbox" style={customStyle}>
            <div className="container__converter">
            <input
                className="container__converter--amount border-color"
                placeholder="Enter Amount"
                value={state.amount}
                type="number"
                onChange={handleInput}
            />
            <div className="container__converter--currency-block flexbox mt">
                <CurrencySelect isLoading={isCurrencyLoading} currencies={currencies} labelName="From" handleChange={handleCurrencyChange}/>
                <CurrencySelect isLoading={isCurrencyLoading} currencies={currencies} labelName="To"  handleChange={handleCurrencyChange}/>
            </div>
            <div className="container__converter--button mt flexbox">
                <button
                    className='container__converter--button-btn border-color'
                    disabled={state.amount === "0" || state.amount === "" || state.amount <= 0 || isCurrencyLoading}
                    onClick={() => convertCurrency()}
                >Convert</button>
            </div>
            <div>
                {isConvertLoading ? <p>Loading ...</p> : <p>{convertAmount} {convertAmount? state.To: ''}</p>}
            </div>
            </div>
        </section>
	);
}

const mapStateToProps = state => ({
    isCurrencyLoading: state.isCurrencyLoading,
    currencies: state.currencies
});

export default connect(mapStateToProps)(Converter);
