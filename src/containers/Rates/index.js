import React from 'react';
import { useQuery } from "react-query";
import { connect } from "react-redux";
import { RateService } from '../../utils/services/rateServices';
import CurrencySelect from '../../components/CurrencySelect';
import { setBaseRate } from '../../actions'
import './index.css';

function Rates({ setBaseRate, isCurrencyLoading, currencies, base_rate }) {
    const { isLoading: isRatesLoading, data: rates } = useQuery('rates', () => RateService.getRates(base_rate || Object.keys(currencies)[0]));

    const handleCurrencyChange = (event)=> {
        setBaseRate(event?.currentTarget?.value || undefined)
    };

	return (
        <section className="container flexbox">
            <div className="container__rates">
            <div className="container__rates--currency flexbox mt">
                <CurrencySelect isLoading={isCurrencyLoading} base_rate={base_rate} currencies={currencies} labelName="Choose your currency" handleChange={handleCurrencyChange}/>
            </div>
                {isRatesLoading ? 
                    <p>Loading ...</p> :
                    <div className="container__rates--table-block mt">
                        <table className="container__rates--table">
                            <thead>
                                <tr>
                                    <th>Currency</th>
                                    <th>Rates</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    Object.keys(rates || {}).map(
                                        index => (
                                            <tr key={index}>
                                                <td>{index}</td>
                                                <td>{rates[index]}</td>
                                            </tr>
                                        )
                                    )
                                }
                            </tbody>
                        </table>
                    </div>
                }
            </div>
        </section>
	);
}

const mapDispatchToProps = {
    setBaseRate
};

const mapStateToProps = state => ({
    isCurrencyLoading: state.isCurrencyLoading,
    currencies: state.currencies,
    base_rate: state.base_rate
});

export default connect(mapStateToProps, mapDispatchToProps)(Rates);