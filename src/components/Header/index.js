import React from 'react';
import { NavLink } from 'react-router-dom';
import './index.css'

function Header() {
    return (
        <header className='header'>
            <nav className='header__nav'>
                <ul className='header__nav--list'>
                    <li>
                        <NavLink
                            to={'/converter'}
                            className={({isActive}) =>
                                "nav-link" + (isActive ? " active" : "")
                            }
                        >
                            <span >Converter</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink
                            to="/rates"
                            className={({isActive}) =>
                                "nav-link" + (isActive ? " active" : "")
                            }
                        >
                            Current Rates
                        </NavLink>
                    </li>
                </ul>
            </nav>
        </header>
    );
}

export default Header;
