import React, { useState } from 'react';
import { connect } from "react-redux";
import CurrencySelect from '../../components/CurrencySelect';
import { setBaseRate } from '../../actions'
import './index.css'

function ModalContainer({ setBaseRate, isCurrencyLoading, currencies, onClose }) {
    const [currency, setCurrency] = useState(undefined)

    const handleCurrencyChange = (event)=> {
        setCurrency(event?.currentTarget?.value)
    };

    const addBaseCurrency = () => {
        setBaseRate(currency || Object.keys(currencies)[0])
        onClose()
    }

    return (
        <div>
            <div className="modal__currency flexbox mt">
                <CurrencySelect isLoading={isCurrencyLoading} currencies={currencies} labelName="Choose your currency" handleChange={handleCurrencyChange}/>
            </div>
            <button
                    className='modal__button-btn border-color mt'
                    disabled={isCurrencyLoading}
                    onClick={() => addBaseCurrency()}
            >
                Submit
            </button>
        </div>
    );
}


const mapDispatchToProps = {
    setBaseRate
};
  
const mapStateToProps = state => ({
    base_rate: state.base_rate,
    currencies: state.currencies,
    isCurrencyLoading: state.isCurrencyLoading
});
  
export default connect(mapStateToProps, mapDispatchToProps)(ModalContainer);
