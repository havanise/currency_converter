import React from 'react';
import './index.css'

export default function CurrencySelect({ base_rate, isLoading, currencies, labelName, handleChange, value}) {
  return (
    <div className='currency'>
        <label>
        {labelName}:
        <select
          className='currency__select'
          value={value}
          onChange={event => handleChange(event, labelName)}
          defaultValue={base_rate}
        >
          {isLoading? <option>load...</option> :Object.keys(currencies || {}).map(currency =>
            <option key={currency}>{currency}</option>
          )}
        </select>
      </label>
    </div>
  );
}