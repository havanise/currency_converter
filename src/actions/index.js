const setData = (type, payload) => {
	return {
		type,
		payload
	}
}

const setCurrencyData = (obj) => {
	return setData('SET_CURRENCY', obj)
}

const setBaseRate = (obj) => {
	return  setData('SET_RATE', obj)
}

export {
	setCurrencyData,
    setBaseRate
}