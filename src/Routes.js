/* eslint-disable import/no-cycle */
import React, { Suspense, lazy, useState, useEffect } from 'react';
import { connect } from "react-redux";
import {
  Routes as BrowserRoutes,
  Route,
  Navigate,
} from 'react-router-dom';
import Modal from './components/Modal';
import { useQuery } from "react-query";
import { CurrencyService } from './utils/services/currencyService';
import ModalContainer from './components/ModalContainer'
import { setCurrencyData } from './actions';

const Converter = lazy(() =>
  /* webpackChunkName: "Converter" */ import('./containers/Converter')
);
const Rates = lazy(() =>
  /* webpackChunkName: "Rates" */ import('./containers/Rates')
);

const mainRoute = '/converter';

function Routes({ setCurrencyData }) {
  const [open, setOpen] = useState(true);
  
  const { isLoading: isCurrencyLoading, data: currencies } = useQuery('currencies', () => CurrencyService.getCurrencies(),  { refetchOnWindowFocus: false });

  useEffect(() => {
    setCurrencyData({isCurrencyLoading, currencies});
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isCurrencyLoading, currencies ]);

  return (
    <>
      <BrowserRoutes>
        <Route exact path={mainRoute} element={<Suspense fallback={<div>Loading... </div>}><Converter /></Suspense>} />
        <Route exact path="/rates"  element={<Suspense fallback={<div>Loading... </div>}><Rates /></Suspense>} />
        <Route exact path="/" element={<Navigate replace to={mainRoute} />} />
      </BrowserRoutes>
      {
        open ?
          <Modal onClose={()=>{setOpen(false)}}>
            <ModalContainer onClose={()=>{setOpen(false)}}/>
          </Modal>
        : null
      }
      </>
  );
}


const mapDispatchToProps = {
  setCurrencyData
};


const mapStateToProps = state => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Routes);