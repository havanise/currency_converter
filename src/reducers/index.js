const INITIAL_STATE = {
	currencies: {},
	base_rate: undefined,
    isCurrencyLoading: false,
}

export function reducer(state = INITIAL_STATE, action) {
    switch(action.type) {
        case "SET_CURRENCY":
            return {
                ...state,
                currencies: action.payload.currencies,
                isCurrencyLoading: action.payload.isCurrencyLoading
            };
        case "SET_RATE":
            return {
                ...state,
                base_rate: action.payload
            };
        default:
            return state;
    }
};
