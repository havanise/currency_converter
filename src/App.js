import Routes from './Routes';
import Header from './components/Header'
import { 
  BrowserRouter
} from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import './App.css';

const queryClient = new QueryClient()

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <div className="App">
        <BrowserRouter>
          <Header />
          <Routes />
        </BrowserRouter>
      </div>
    </QueryClientProvider>
  );
}

export default App;
